//
//  AppDelegate.swift
//  SampleFlickerApp
//
//  Created by Linganna on 06/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.configureAppFlow()
        return true
    }
    
    func configureAppFlow() {
        
        let flickerAuthToken = UserDefaults.standard.object(forKey: "auth_token")
        
        if let _ = flickerAuthToken {
            
            let progressIndictor = UIActivityIndicatorView(frame: (self.window?.frame)!)
            progressIndictor.activityIndicatorViewStyle = .gray
            progressIndictor.color = UIColor.darkGray
            progressIndictor.hidesWhenStopped = true
            self.window?.addSubview(progressIndictor)
            progressIndictor.startAnimating()
            
            let photosDisplayer = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotosDisplayer") as? UINavigationController
            let photosDisplayerContainer = photosDisplayer?.topViewController as? PhotosDisplayerViewController
            
            self.window?.rootViewController = photosDisplayer

            
            
            FlickerHTTPManager.sharedInstance.downloadImages(ownUploaded: false, page: 1, batchCount: 25, completionBlock: { (downLoadedPhotos) in
                
                DispatchQueue.main.async {
                    progressIndictor.stopAnimating()
                    progressIndictor.removeFromSuperview()
                    photosDisplayerContainer?.photos = downLoadedPhotos
                    photosDisplayerContainer?.result.insert(photosDisplayerContainer?.photos as AnyObject, at: 0)
                    photosDisplayerContainer?.photosColletion.reloadData()
                }
                
            })

            
        }else if !(self.window?.rootViewController is ViewController) {
            
            let firstViewControler = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstViewController") as? UINavigationController

             self.window?.rootViewController = firstViewControler
        }
        
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


    

}

