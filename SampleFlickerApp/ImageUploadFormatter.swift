//
//  ImageUploadFormatter.swift
//  SampleFlickerApp
//
//  Created by Linganna on 07/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import Foundation
import MobileCoreServices
import AVFoundation
import UIKit

class ImageUploadFormatter :NSObject {
    
    let apiSeg = "api_key\(FlickerBaseURL.apiKey)auth_token\(UserDefaults.standard.object(forKey: "auth_token"))formatjsonnojsoncallback1"
    func imageDataFormatter(image:String) -> Data?{
        let params = ["api_key" : FlickerBaseURL.apiKey,
                      "auth_token" : UserDefaults.standard.object(forKey: "auth_token"),
                      "api_sig": FlickerUtils.createMD5(apiSeg),
                      "format":"json", "nojsoncallback": 1]
        return self.createBodyWithParameters(parameters: params as? [String:String], filePathKey: "photo", paths: [image])
        
    }
  
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, paths: [String]?) -> Data? {
        var body = Data()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.append("--\(generateBoundaryString())\r\n".data(using: .utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                body.append("\(value)\r\n".data(using: .utf8)!)
            }
        }
        
        if paths != nil {
            for path in paths! {
                let url = URL(fileURLWithPath: path)
                let filename = url.lastPathComponent
                do {
                    let data = try Data(contentsOf: url)
                    let mimetype = self.mimeType(for: path)
                    
                    body.append("--\(generateBoundaryString())\r\n".data(using: .utf8)!)
                    body.append("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
                    body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
                    body.append(data)
                    body.append("\r\n".data(using: .utf8)!)

                } catch  {
                    
                }
            }
        }

        body.append("--\(generateBoundaryString())--\r\n".data(using: .utf8)!)
        return body
    }
    
    
    public func generateBoundaryString() -> String {
        return "--\(NSUUID().uuidString)"
    }
    
    func mimeType(for path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream";
    }
    
    
  
}

