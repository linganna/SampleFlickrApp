//
//  ViewController.swift
//  SampleFlickerApp
//
//  Created by Linganna on 06/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userPrompt: UILabel!
    
    @IBOutlet weak var authorizeButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var pinCode: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    
    }
  
    @IBAction func loginButtonPressed(_ sender: AnyObject) {
        
        if !self.pinCode.text.isEmpty {
            
            let progressIndictor = UIActivityIndicatorView(frame: (self.view.window?.frame)!)
            progressIndictor.activityIndicatorViewStyle = .gray
            progressIndictor.color = UIColor.darkGray
            progressIndictor.hidesWhenStopped = true
            self.view.window?.addSubview(progressIndictor)
            progressIndictor.startAnimating()
            
            FlickerHTTPManager.sharedInstance.passCode = self.pinCode.text
            
            FlickerHTTPManager.sharedInstance.completeAuthentication(completionBlock: { (result) in
                if result! {
                    
                    FlickerHTTPManager.sharedInstance.downloadImages(ownUploaded: false, page: 1, batchCount: 25, completionBlock: { (downLoadedPhotos) in
                        
                        DispatchQueue.main.async {
                            
                            progressIndictor.stopAnimating()
                            let photosDisplayer = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PhotosDisplayer") as? UINavigationController
                            let photosDisplayerContainer = photosDisplayer?.topViewController as? PhotosDisplayerViewController
                            
                            photosDisplayerContainer?.photos = downLoadedPhotos
                             photosDisplayerContainer?.result.insert(photosDisplayerContainer?.photos as AnyObject, at: 0)
                            self.present(photosDisplayer!, animated: true, completion: {
                                
                            })
                        }

                    })
                }
            })
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        self.userPrompt.isHidden = true
        self.pinCode.isHidden = false
        self.loginButton.isHidden = false
        self.authorizeButton.isHidden = true
        
    }
 

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
}

