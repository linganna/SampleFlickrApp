//
//  FlickerConstant.swift
//  SampleFlickerApp
//
//  Created by Linganna on 06/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import Foundation

struct FlickerBaseURL {
    
    static let apiKey = "ac5257ce455e2fdc05f358898159d889"
    static let secretKey = "3f6a0bd704de9928"
    static let baseURL = "https://api.flickr.com/services/rest/?method="
    static let  authenticationString = "http://flickr.com/services/auth/?"
    
    static let permissionURL = "https://www.flickr.com/auth-72157676129773160"
    
    static let getTokenMethod = "flickr.auth.getToken"
    static let downLoadMethod = "flickr.photos.search"
    
    static let getFullToken = "flickr.auth.getFullToken"
    
    static let photoUploadURL = "https://api.flickr.com/services/upload/"
    
    static let authToken = "auth_token"
    static let userId = "user_id"
}
