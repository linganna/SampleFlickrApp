//
//  FlickerUtils.swift
//  SampleFlickerApp
//
//  Created by Linganna on 07/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import Foundation
import UIKit

class FlickerUtils :NSObject{
    
    
    class func createMD5(_ string: String) -> String? {
        let context = UnsafeMutablePointer<CC_MD5_CTX>.allocate(capacity: 1)
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5_Init(context)
        CC_MD5_Update(context, string,
                      CC_LONG(string.lengthOfBytes(using: String.Encoding.utf8)))
        CC_MD5_Final(&digest, context)
        context.deallocate(capacity: 1)
        var hexString = ""
        for byte in digest {
            hexString += String(format:"%02x", byte)
        }
        
        return hexString
    }
    
    
    class func createTinyThumbnail(_ image:UIImage) -> UIImage
    {
        
        let newWidth:CGFloat = 100.0
        let newHeight:CGFloat = round((image.size.height/image.size.width) * newWidth)
        let newRect = CGRect(x: 0.0, y: 0.0, width: newWidth, height: newHeight).integral
        
        UIGraphicsBeginImageContext(newRect.size)
        let context = UIGraphicsGetCurrentContext()
        
        // Set the quality level to use when rescaling
        context!.interpolationQuality = CGInterpolationQuality.high
        // Draw into the context; this scales the image
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        var data:NSData? = UIImageJPEGRepresentation(newImage!, 0.5)! as NSData?
        var imageQuality:CGFloat = 0.8
        while data!.length > 3000 && imageQuality > 0.3{
            let tempImage = UIImage(data: data! as Data)
            data = UIImageJPEGRepresentation(tempImage!, imageQuality)! as NSData?
            imageQuality = imageQuality - 0.1
        }
        
        return newImage!
    }
    
    
    class func getCurrentMilliseconds() -> Int64
    {
        var time = timeval()
        gettimeofday(&time, nil)
        let timestamp = Int64(time.tv_sec) * 1000 + Int64(time.tv_usec) / 1000
        return timestamp
    }
}
