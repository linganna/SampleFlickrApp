//
//  UserActionsCell.swift
//  SampleFlickerApp
//
//  Created by Linganna on 08/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import UIKit

class UserActionsCell: UITableViewCell {

    @IBOutlet weak var userAction: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
