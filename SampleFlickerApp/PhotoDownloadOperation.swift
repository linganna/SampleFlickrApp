//
//  PhotoDownloadOperation.swift
//  SampleFlickerApp
//
//  Created by Linganna on 10/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import UIKit

class PhotoDownloadOperation: Operation {

    var downloadURL:URL?
    
    
    private var _executing: Bool = false
    override var isExecuting: Bool {
        get {
            return _executing
        }
        set {
            if _executing != newValue {
                willChangeValue(forKey: "isExecuting")
                _executing = newValue
                didChangeValue(forKey: "isExecuting")
            }
        }
    }
    
    private var _finished: Bool = false
    override var isFinished: Bool {
        get {
            return _finished
        }
        set {
            if _finished != newValue {
                willChangeValue(forKey: "isFinished")
                _finished = newValue
                didChangeValue(forKey: "isFinished")
            }
        }
    }
    
    init(url:URL?) {
        
        self.downloadURL =  url
        super.init()
    }
    override func start() {

        if self.isCancelled {
            self.done()
        }else{
            self._executing = true
            
            let downloadTask = FlickerHTTPManager.sharedInstance.session?.downloadTask(with: self.downloadURL!, completionHandler: { (downloadedLocation, urlResponse, error) in
                if let _ = error {
                    
                }else{
                    
                }
                self.done()
            })
            
            downloadTask?.resume()
        }
        
    }
    
    func done(){
        
        
        self.isExecuting = false
        self.isFinished = true
    }
    

}
