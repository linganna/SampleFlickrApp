//
//  FlickerHTTPManager.swift
//  SampleFlickerApp
//
//  Created by Linganna on 06/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import Foundation


class FlickerHTTPManager: NSObject {
    
    static let sharedInstance = FlickerHTTPManager()
    
    
    var passCode:String?
    var session:URLSession?
    
    
    override init() {
        session = URLSession(configuration: .default, delegate: nil, delegateQueue: nil)
        
    }

    func completeAuthentication(completionBlock:@escaping (_ result:Bool?)->Void){
     
        
        var api_sig  = "\(FlickerBaseURL.secretKey)api_key\(FlickerBaseURL.apiKey)formatjsonmethod\(FlickerBaseURL.getFullToken)mini_token\(passCode)nojsoncallback1"
        
        
        api_sig = FlickerUtils.createMD5(api_sig)!
        
        var authenticationString = FlickerBaseURL.baseURL+FlickerBaseURL.getFullToken
        authenticationString += "&api_key=\(FlickerBaseURL.apiKey)&format=json&nojsoncallback=1&mini_token=\(passCode)&api_sig=\(api_sig)"
        
        let urlwithPercentEscapes = authenticationString.addingPercentEncoding( withAllowedCharacters: NSCharacterSet.urlQueryAllowed)

        var request = URLRequest(url: URL(string:urlwithPercentEscapes!)!)
        request.httpMethod = "GET"
        
    
        let task =  session?.dataTask(with: request){ data,URLResponse,error in
             do {
                if let _ = data {
                    let response = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]
                    if  let auth = response?["auth"] as? [String:Any], let tokenContent = auth["token"] as? [String:Any], let token =  tokenContent["_content"] {
                        
                        let userInfo = auth["user"] as? [String:Any]
                        UserDefaults.standard.set(token, forKey: FlickerBaseURL.authToken)
                        UserDefaults.standard.set(userInfo?["nsid"], forKey: FlickerBaseURL.userId)

                        UserDefaults.standard.synchronize()
                        completionBlock(true)
                    }else{
                         completionBlock(false)
                    }
                    
                }else{
                     completionBlock(false)
                }

             }catch{
                completionBlock(false)
            }
            
        }
        task?.resume()
    }
    
    
    func uploadPhoto(imagePath:String, contentLenght:String, completionBlock:@escaping (_ result:Bool, _ response:Error?)->Void) {
        
        let uploadString = FlickerBaseURL.photoUploadURL
        let requestBody = ImageUploadFormatter()
        
        var request = URLRequest(url: URL(string:uploadString)!)
 
        
        request.setValue("multipart/form-data; boundary=\(requestBody.generateBoundaryString())", forHTTPHeaderField: "Content-Type")
        request.setValue(contentLenght, forHTTPHeaderField: "Content-Length")
        request.httpBody = requestBody.imageDataFormatter(image: imagePath)
        request.httpMethod = "POST"
        
        let dataTask = session?.dataTask(with: request, completionHandler: { (data, urlResponse, error) in
            do {
                if let _ = data {
                    let response =  try JSONSerialization.jsonObject(with:data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                      completionBlock(true, nil)
                    print(response)
                }else{
                    completionBlock(false,error!)
                }
            }catch{
                completionBlock(false,nil)
            }
            
        })
        
        dataTask?.resume()
        
    }
    
    func downloadImages(ownUploaded:Bool?,page:Int,batchCount:Int,completionBlock:@escaping (_ photos:[PhotoInfo]?)->Void) {
        
        let authToken = UserDefaults.standard.object(forKey: "auth_token")
        
        let apiSig = "\(FlickerBaseURL.secretKey)api_key\(FlickerBaseURL.apiKey)auth_token\(authToken!)formatjsonmethodflickr.photos.searchnojsoncallback1page\(page)per_page\(batchCount)"
        let api_sig = FlickerUtils.createMD5(apiSig)
        var downlaodURL = FlickerBaseURL.baseURL+FlickerBaseURL.downLoadMethod
        downlaodURL +=  "&api_key=\(FlickerBaseURL.apiKey)&per_page=\(batchCount)&format=json&nojsoncallback=1&page=\(page)&auth_token=\(authToken!)&api_sig=\(api_sig!)"
        
        if ownUploaded == true{
            downlaodURL += "&user_id=me)"
        }
        
        var request = URLRequest(url: URL(string:downlaodURL)!)
        request.httpMethod = "GET"
        
        
        var publicPhotos = [PhotoInfo]()
        
        let task =  session?.dataTask(with: request){ data,URLResponse,error in
            if let _ = data {
                
                do {
                    let response =  try JSONSerialization.jsonObject(with:data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if let photosResponse = response as? [String: Any], let allPhotosInfo = photosResponse["photos"] as? [String: Any] , let photos = allPhotosInfo["photo"] as? [[String:Any]] {
                        
                        for photo in photos {
                            publicPhotos.append(PhotoInfo(info: photo))
                        }
                    }
                    completionBlock(publicPhotos)

                }catch{
                    
                }
            }else{
                 completionBlock(nil)
            }
        }
        task?.resume()

    }
    
    internal func apiCallSignatueString(paramters:[String:String]) -> String? {
        var apiSig = ""
        for (key,value) in paramters {
            apiSig += "\(key)\(value)"
        }
        return apiSig
    }
    
    
    
}
