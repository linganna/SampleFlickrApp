//
//  PhotoInfo.swift
//  SampleFlickerApp
//
//  Created by Linganna on 06/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import Foundation

class PhotoInfo:NSObject {
    
    var title:String?
    var photoURL:URL?
    
    
    init(info:[String:Any]) {
        
        self.title = info["title"] as! String?
        
        // NSString *photoURLString = [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%@_%@_s.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];

        let photoURLString = "https://farm\(info[("farm")]!).staticflickr.com/\(info["server"] as! String)/\(info["id"] as! String)_\(info["secret"] as! String)_s.jpg"
        
        self.photoURL = URL(string: photoURLString)
        
        
    }
    
    
}
