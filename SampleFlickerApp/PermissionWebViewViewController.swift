//
//  PermissionWebViewViewController.swift
//  SampleFlickerApp
//
//  Created by Linganna on 07/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import UIKit

class PermissionWebViewViewController: UIViewController {

    @IBOutlet weak var authenticatoinWebView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: FlickerBaseURL.permissionURL)
        let urlRequest = NSMutableURLRequest(url: url!, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 30)
        self.authenticatoinWebView.loadRequest(urlRequest as URLRequest)

        
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
  
}
