//
//  PhotosDisplayerViewController.swift
//  SampleFlickerApp
//
//  Created by Linganna on 06/12/16.
//  Copyright © 2016 Linganna. All rights reserved.
//

import UIKit

class PhotosDisplayerViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var photosTable: UITableView!
    @IBOutlet weak var photosSelector: UISegmentedControl!
    
    @IBOutlet weak var photosColletion: UICollectionView!
    
    var photos:[PhotoInfo]?
    
    var result = [AnyObject]()
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 40.0, right: 10.0)

    
    fileprivate let itemsPerRow: CGFloat = 4
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flickerAuthToken = UserDefaults.standard.object(forKey: "auth_token")
        
        if let _ = flickerAuthToken  {
            
            let uploadButton = UIBarButtonItem(title: "UploadPhoto", style: .plain, target: self, action:#selector(uploadPhotos))
            let logOutButton = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logout))
            self.navigationItem.leftBarButtonItem = uploadButton
            self.navigationItem.rightBarButtonItem = logOutButton
            
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
    @IBAction func displayPhotos(_ sender: AnyObject){
        
        let selectedSource = (sender as! UISegmentedControl).selectedSegmentIndex
        
        let progressIndictor = UIActivityIndicatorView(frame: (self.view.window?.frame)!)
        progressIndictor.activityIndicatorViewStyle = .gray
        progressIndictor.color = UIColor.darkGray
        progressIndictor.hidesWhenStopped = true
        self.view.window?.addSubview(progressIndictor)
        progressIndictor.startAnimating()

        var ownPhotos = false
        
        if selectedSource == 1{
            ownPhotos = true

        }
        
        FlickerHTTPManager.sharedInstance.downloadImages(ownUploaded: ownPhotos, page: 1, batchCount: 25, completionBlock: { (downLoadedPhotos) in
            
            DispatchQueue.main.async {
                progressIndictor.stopAnimating()
                progressIndictor.removeFromSuperview()
                self.photos = downLoadedPhotos
                self.result.insert(self.photos as AnyObject, at: 0)
                self.photosColletion.reloadData()
            }
            
        })
        
        
    }
    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return ( photos != nil ? (photos?.count)! : 0)
    }
    
     func collectionView(_ collectionView: UICollectionView,
                                 numberOfItemsInSection section: Int) -> Int {
        return result[section].self.photos!!.count
    }
    
     func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoDisplayCell",
                                                      for: indexPath) as! PhotoDisplayCell
        cell.backgroundColor = UIColor.black
        // Configure the cell
        let photoInfo = result[(indexPath as NSIndexPath).section].self.photos??[(indexPath as NSIndexPath).row]
        
        print("PhototURL:\(photoInfo?.photoURL) at \(indexPath.section)")
        
        let imageData = try? Data(contentsOf: (photoInfo?.photoURL)! as URL)
        let image = UIImage(data: imageData!)
        cell.photoView.image = image

        
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    
    func uploadPhotos() {
        
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let showPhotos = UIAlertAction(title: "PhotoLibrary", style: .default) { action in
            
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            self.present(picker, animated: true, completion: {
                
            })
            
        }
        
        let takePic =  UIAlertAction(title: "TakePhoto", style: .default) { action in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.allowsEditing = true
            picker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(picker, animated: true, completion: {
                
            })
        }
        sheet.addAction(showPhotos)
        sheet.addAction(takePic)
        
        self.present(sheet, animated: true) { 
            
        }

    }
    
     public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    
        let image = info[UIImagePickerControllerEditedImage]
       
        let smallImage = FlickerUtils.createTinyThumbnail(image as! UIImage)
        
        let imageData = UIImageJPEGRepresentation(smallImage,0.5)
        
        let documentsDirPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray)[0] as! String
        let localImagePath = documentsDirPath.appending("/image_\(FlickerUtils.getCurrentMilliseconds()).jpg")
        let imageURL = URL(fileURLWithPath: localImagePath)
        
        do {
            try imageData?.write(to: imageURL)
        } catch  {
            
        }
        
        FlickerHTTPManager.sharedInstance.uploadPhoto(imagePath: imageURL.path, contentLenght: String(describing: (imageData?.count)!)) { (result, error) in
            
            DispatchQueue.main.async {
                picker.dismiss(animated: true) {
                    
                }
            }
        }
        
    }
    
     public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true) { 
            
        }
        
    }

    func logout() {
        
        UserDefaults.standard.removeObject(forKey:  FlickerBaseURL.authToken)
        UserDefaults.standard.removeObject(forKey:  FlickerBaseURL.userId)
        self.dismiss(animated: true) { 
            let appDelete = UIApplication.shared.delegate as! AppDelegate
            appDelete.configureAppFlow()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

